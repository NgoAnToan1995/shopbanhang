-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 23, 2020 lúc 08:32 AM
-- Phiên bản máy phục vụ: 10.1.39-MariaDB
-- Phiên bản PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `shopbanhang`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cart`
--

CREATE TABLE `cart` (
  `id` int(10) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `promotion` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `url`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Áo Nam', '#', NULL, 1, '2020-07-21', '2020-07-21'),
(2, 1, 'Áo sơ mi', '#', 'Áo sơ mi', 1, '2020-07-21', '2020-07-22'),
(4, 0, 'Ví da', '#', 'Ví da', 1, '2020-07-22', '2020-07-22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `price` int(20) NOT NULL,
  `promotion` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `code`, `description`, `price`, `promotion`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 2, 'ÁO SƠ MI TRẮNG', 'ASM1266', '<p>&Aacute;o sơ mi trắng với thiết kế đơn giản, trẻ trung t&ocirc;n l&ecirc;n vẻ thanh lịch, trưởng th&agrave;nh của ph&aacute;i mạnh.</p>', 245000, 0, 'http://shopbanhang.com/public/uploads/images/%C3%A1o-s%C6%A1-mi/ao-so-mi-trang-asm1266-10790-slide-products-5cedf46614ee3.png', 1, '2020-07-22', '2020-07-22'),
(3, 2, 'ÁO SƠ MI CARO KEM', 'ASM1272', '<p>&Aacute;o sơ mi caro l&agrave;m bật l&ecirc;n sự trẻ trung, năng động. M&agrave;u kem tinh giản khiến ph&aacute;i mạnh c&agrave;ng trở n&ecirc;n lịch l&atilde;m.<br />\r\nMột sản phẩm mới của 4MEN rất th&iacute;ch hợp trong m&ugrave;a h&egrave;.</p>', 295000, 10, 'http://shopbanhang.com/public/uploads/images/%C3%A1o-s%C6%A1-mi/ao-so-mi-caro-kem-asm1272-10784-slide-products-5cedf603a5ec2.png', 1, '2020-07-22', '2020-07-22'),
(4, 2, 'ÁO SƠ MI OXFORD TAY NGẮN', 'ASM017', '<p>Chất liệu: Oxford 100% cotton.<br />\r\nForm: Regular<br />\r\nĐặc t&iacute;nh: &nbsp;tho&aacute;ng m&aacute;t, bền m&agrave;u<br />\r\nHướng dẫn sử dụng:<br />\r\n- Giặt ở nhiệt độ b&igrave;nh thường, với đồ c&oacute; m&agrave;u tương tự.<br />\r\n- Kh&ocirc;ng được d&ugrave;ng h&oacute;a chất tẩy.<br />\r\n- Hạn chế sử dụng m&aacute;y sấy, ủi ở nhiệt độ b&igrave;nh thường.</p>', 245000, 20, 'http://shopbanhang.com/public/uploads/images/%C3%A1o-s%C6%A1-mi/ao-so-mi-tay-ngan-lung-in-asm013-15391-slide-products-5f0d59ad6d2d7.png', 1, '2020-07-22', '2020-07-22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products_attributes`
--

CREATE TABLE `products_attributes` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double(10,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products_attributes`
--

INSERT INTO `products_attributes` (`id`, `product_id`, `sku`, `size`, `price`, `stock`, `created_at`, `updated_at`) VALUES
(5, 2, 'ASM1266-M', 'M', 245000.00, 5, '2020-07-22', '2020-07-22'),
(6, 2, 'ASM1266-L', 'L', 250000.00, 5, '2020-07-22', '2020-07-22'),
(7, 2, 'ASM1266-XL', 'XL', 255000.00, 5, '2020-07-22', '2020-07-22'),
(8, 3, 'ASM1272-M', 'M', 295000.00, 5, '2020-07-22', '2020-07-22'),
(9, 3, 'ASM1272-L', 'L', 300000.00, 5, '2020-07-22', '2020-07-22'),
(10, 3, 'ASM1272-XL', 'XL', 305000.00, 5, '2020-07-22', '2020-07-22'),
(11, 4, 'ASM017-S', 'S', 245000.00, 5, '2020-07-22', '2020-07-22'),
(12, 4, 'ASM017-M', 'M', 250000.00, 5, '2020-07-22', '2020-07-22'),
(13, 4, 'ASM017-L', 'L', 255000.00, 5, '2020-07-22', '2020-07-22'),
(14, 4, 'ASM017-XL', 'XL', 260000.00, 5, '2020-07-22', '2020-07-22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products_colors`
--

CREATE TABLE `products_colors` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double(10,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products_colors`
--

INSERT INTO `products_colors` (`id`, `product_id`, `sku`, `color`, `price`, `stock`, `created_at`, `updated_at`) VALUES
(5, 2, 'ASM1266-T', 'TRẮNG', 245000.00, 5, '2020-07-22', '2020-07-22'),
(6, 2, 'ASM1266-D', 'ĐEN', 250000.00, 5, '2020-07-22', '2020-07-22'),
(7, 3, 'ASM1272-K', 'KEM', 295000.00, 5, '2020-07-22', '2020-07-22'),
(8, 4, 'ASM017-X', 'XANH', 245000.00, 5, '2020-07-22', '2020-07-22'),
(9, 4, 'ASM017-T', 'TRẮNG', 250000.00, 5, '2020-07-22', '2020-07-22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products_images`
--

CREATE TABLE `products_images` (
  `id` int(10) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products_images`
--

INSERT INTO `products_images` (`id`, `product_id`, `image`, `created_at`, `updated_at`) VALUES
(6, 2, 'http://shopbanhang.com/public/uploads/images/%C3%A1o-s%C6%A1-mi/ao-so-mi-trang-asm1266-10790-slide-products-5cedf46614ee3.png', '2020-07-22', '2020-07-22'),
(7, 2, 'http://shopbanhang.com/public/uploads/images/%C3%A1o-s%C6%A1-mi/ao-so-mi-trang-asm1266-10790-slide-products-5cedf46c6089d.png', '2020-07-22', '2020-07-22'),
(8, 2, 'http://shopbanhang.com/public/uploads/images/%C3%A1o-s%C6%A1-mi/ao-so-mi-trang-asm1266-10790-slide-products-5cedf46c995ab.png', '2020-07-22', '2020-07-22'),
(11, 3, 'http://shopbanhang.com/public/uploads/images/%C3%A1o-s%C6%A1-mi/ao-so-mi-caro-kem-asm1272-10784-slide-products-5cedf603a5ec2.png', '2020-07-22', '2020-07-22'),
(12, 3, 'http://shopbanhang.com/public/uploads/images/%C3%A1o-s%C6%A1-mi/ao-so-mi-caro-kem-asm1272-10784-slide-products-5cedf60402b7a.png', '2020-07-22', '2020-07-22'),
(13, 3, 'http://shopbanhang.com/public/uploads/images/%C3%A1o-s%C6%A1-mi/ao-so-mi-caro-kem-asm1272-10784-slide-products-5cedf5fd0861a.png', '2020-07-22', '2020-07-22'),
(14, 4, 'http://shopbanhang.com/public/uploads/images/%C3%A1o-s%C6%A1-mi/ao-so-mi-tay-ngan-lung-in-asm013-15391-slide-products-5f0d59ad6d2d7.png', '2020-07-22', '2020-07-22'),
(15, 4, 'http://shopbanhang.com/public/uploads/images/%C3%A1o-s%C6%A1-mi/ao-so-mi-tay-ngan-lung-in-asm013-15391-slide-products-5f0d59ade9fa5.png', '2020-07-22', '2020-07-22'),
(16, 4, 'http://shopbanhang.com/public/uploads/images/%C3%A1o-s%C6%A1-mi/ao-so-mi-tay-ngan-lung-in-asm013-15391-slide-products-5f0d59ae8399f.png', '2020-07-22', '2020-07-22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1: admin. 0: người dùng',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `admin`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$7c/EdSI9QDnv4KNszJsbDeyeTGOBtlSz11658NQIl0eLbRAf1Fhoe', 1, NULL, '2020-07-21 07:49:50', '2020-07-21 07:49:50');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products_attributes`
--
ALTER TABLE `products_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products_colors`
--
ALTER TABLE `products_colors`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products_images`
--
ALTER TABLE `products_images`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `products_attributes`
--
ALTER TABLE `products_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `products_colors`
--
ALTER TABLE `products_colors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `products_images`
--
ALTER TABLE `products_images`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
