<?php
namespace Tests;

use App\SapXep;
use PHPUnit\Framework\TestCase;

class MyTest extends TestCase
{
    public function testMy()
    {
        $mytest = new SapXep();
        
        $this->assertEquals("9534330", $mytest->So_Lon_Nhat([3,30,34,5,9]));
    }
}