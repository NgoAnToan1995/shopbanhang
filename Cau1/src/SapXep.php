<?php
namespace App;
class SapXep
{
    function myCompare($X, $Y)  
    {   
        $XY = $Y.$X;  
        $YX = $X.$Y;  
        return strcmp($XY, $YX) > 0 ? 1: 0;  
    }  

    function So_Lon_Nhat($arr)  
    {
        $a = '';
        usort($arr, array($this,'myCompare'));
        for ($i = 0; $i < count($arr) ; $i++ )  
            $a .=  $arr[$i];
        return $a;
    }  
}