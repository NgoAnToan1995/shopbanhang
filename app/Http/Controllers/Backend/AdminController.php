<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;

class AdminController extends Controller
{
    public function login(Request $request)
    {
        if (Auth::check()) {
            return redirect('admin/dashboard');
        }
        if ($request->isMethod('post')) {
            $data = $request->input();
            if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'admin' => '1'])) {
                return redirect('admin/dashboard');
            } else {
                return redirect('admin')->with('flash_message_error', 'Email hoặc Mật khẩu không đúng');
            }
        }
        return view('backend.login');
    }

    public function dashboard()
    {
        return view('backend.dashboard');
    }

    public function logout()
    {
        Session::flush();
        return redirect('/admin')->with('flash_message_success','Đăng xuất thành công!');
    }
}
