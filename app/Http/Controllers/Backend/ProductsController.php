<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Products;
use App\ProductsAttributes;
use App\ProductsImages;
use App\ProductsColors;

class ProductsController extends Controller
{
// Controller Product
    public function addProduct(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'category_id'           => 'required',
                'product_name'          => 'required',
                'product_code'          => 'required',
                'product_description'   => 'string',
                'product_price'         => 'required|numeric',
                'promotion'             => 'numeric',
                'product_image'         => 'required'
            ],[
                'category_id.required'          => 'Danh mục sản phẩm không được trống',
                'product_name.required'         => 'Tên sản phẩm không được trống',
                'product_code.required'         => 'Mã sản phẩm không được trống',
                'product_description.string'    => 'Mô tả sản phẩm không được trống',
                'product_price.required'        => 'Giá sản phẩm không được trống',
                'product_price.numeric'         => 'Giá sản phẩm phải là số',
                'promotion.numeric'             => '% giảm giá phải là số',
                'product_image.required'        => 'Ảnh sản phẩm không được trống'
            ]);

            $data = $request->all();
            $product = new Products;
            $product->category_id   = $data['category_id'];
            $product->name          = $data['product_name'];
            $product->code          = $data['product_code'];
            $product->description   = $data['product_description'];
            $product->price         = $data['product_price'];
            $product->promotion     = $data['promotion'];
            $product->image         = $data['product_image'];
            $product->save();
            return redirect('/admin/view-products')->with('flash_message_success', 'Thêm sản phẩm thành công!');
        }
        // Dropdown danh mục sản phẩm
        $categories = Category::where(['parent_id' => 0])->get();
        $categories_dropdown = "<option value='' selected>Select</option>";
        foreach ($categories as $cat) {
            $categories_dropdown .= "<option style='font-weight: 700;' value='".$cat->id."'>".$cat->name."</option>";
            $sub_categories = Category::where(['parent_id'=>$cat->id])->get();
            foreach ($sub_categories as $sub_cat) {
                $categories_dropdown .= "<option value='".$sub_cat->id."'>&nbsp;---&nbsp".$sub_cat->name."</option>";
            }
        }
        return view('backend.products.add_product')->with(compact('categories_dropdown'));
    }

    public function viewProducts()
    {
        $products = Products::get();
        return view('backend.products.view_products')->with(compact('products'));
    }

    public function editProduct(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'category_id'           => 'required',
                'product_name'          => 'required',
                'product_code'          => 'required',
                'product_description'   => 'string',
                'product_price'         => 'required|numeric',
                'promotion'             => 'numeric',
                'product_image'         => 'required'
            ],[
                'category_id.required'          => 'Danh mục sản phẩm không được trống',
                'product_name.required'         => 'Tên sản phẩm không được trống',
                'product_code.required'         => 'Mã sản phẩm không được trống',
                'product_description.string'    => 'Mô tả sản phẩm không được trống',
                'product_price.required'        => 'Giá sản phẩm không được trống',
                'product_price.numeric'         => 'Giá sản phẩm phải là số',
                'promotion.numeric'             => '% giảm giá phải là số',
                'product_image.required'        => 'Ảnh sản phẩm không được trống'
            ]);
            $data = $request->all();
            Products::where(['id' => $id])->update([
                'name'          => $data['product_name'],
                'category_id'   => $data['category_id'],
                'code'          => $data['product_code'],
                'description'   => $data['product_description'],
                'price'         => $data['product_price'],
                'promotion'     => $data['promotion'],
                'image'         => $data['product_image']
            ]);
            return redirect('/admin/view-products')->with('flash_message_success', 'Cập nhật sản phẩm thành công!');
        }

        $productDetails = Products::where(['id' => $id])->first();
        // Dropdown danh mục sản phẩm
        $categories = Category::where(['parent_id' => 0])->get();
        $categories_dropdown = "<option value='' selected disabled>Select</option>";
        foreach ($categories as $cat) {
            if ($cat->id == $productDetails->category_id) {
                $selected = "selected";
            } else {
                $selected = "";
            }
            $categories_dropdown .= "<option style='font-weight: 700;' value='".$cat->id."'".$selected.">".$cat->name."</option>";

            // Dropdown danh mục con sản phẩm
            $sub_categories = Category::where(['parent_id' => $cat->id])->get();
            foreach ($sub_categories as $sub_cat) {
                if ($sub_cat->id == $productDetails->category_id) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                $categories_dropdown .= "<option value='".$sub_cat->id."'".$selected.">&nbsp;--&nbsp".$sub_cat->name."</option>";
            }
        }
        return view('backend.products.edit_product')->with(compact('productDetails', 'categories_dropdown'));
    }

    public function updateStatus(Request $request, $id = null)
    {
        $data = $request->all();
        Products::where('id', $data['id'])->update(['status' => $data['status']]);
    }

    public function deleteProduct($id = null)
    {
        Products::where('id', $id)->delete();
        ProductsImages::where('product_id', $id)->delete();
        ProductsAttributes::where('product_id', $id)->delete();
        ProductsColors::where('product_id', $id)->delete();
        return redirect()->back()->with('flash_message_error', 'Xóa sản phẩm thành công');
    }

// Controller Product Attributes
    public function addAttributes(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            foreach ($data['sku'] as $key => $val) {
                if (!empty($val)) {
                    // Kiểm tra SKU đã tồn tại chưa
                    $attrCountSKU = ProductsAttributes::where('sku', $val)->count();
                    if ($attrCountSKU > 0) {
                        return redirect('/admin/add-attributes/'. $id)->with('flash_message_error', 'SKU đã tồn tại, vui lòng chon SKU khác!');
                    }
                    // Kiểm tra size có bị trùng không
                    $attrCountSizes = ProductsAttributes::where(['product_id' => $id, 'size' => $data['size'][$key]])->count();
                    if ($attrCountSizes > 0) {
                        return redirect('/admin/add-attributes/'. $id)->with('flash_message_error', ''.$data['size'][$key].' Size đã tồn tại vui lòng chọn size khác!');
                    }

                    $attribute = new ProductsAttributes;
                    $attribute->product_id  = $id;
                    $attribute->sku         = $val;
                    $attribute->size        = $data['size'][$key];
                    $attribute->price       = $data['price'][$key];
                    $attribute->stock       = $data['stock'][$key];
                    $attribute->save();
                }
            }
            return redirect('/admin/add-attributes/'. $id)->with('flash_message_success','Thêm thuộc tính thành công!');
        }
        $productDetails = Products::with('attributes')->where(['id' => $id])->first();
        return view('backend.products.add_attributes')->with(compact('productDetails'));
    }

    public function editAttributes(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            foreach ($data['attr'] as $key => $attr) {
                ProductsAttributes::where(['id' => $data['attr'][$key]])->update([
                    'sku'   => $data['sku'][$key],
                    'size'  => $data['size'][$key],
                    'price' => $data['price'][$key],
                    'stock' => $data['stock'][$key],
                ]);
            }
            return redirect()->back()->with('flash_message_success', 'Cập nhật thuộc tính thành công!');
        }
    }

    public function deleteAttribute($id = null)
    {
        ProductsAttributes::where(['id' => $id])->delete();
        return redirect()->back()->with('flash_message_error', 'Xóa thuộc tính thành công!');
    }

// Controller Slider Image Product
    public function addImages(Request $request, $id = null)
    {
        $productDetails = Products::with('attributes')->where(['id' => $id])->first();
        if ($request->isMethod('post')) {
            $data = $request->all();
            if (!empty($data['image'])) {
                $image = new ProductsImages;
                $image->product_id = $id;
                $image->image = $data['image'];
                $image->save();
                return redirect('/admin/add-images/'.$id)->with('flash_message_success', 'Thêm ảnh thành công');
            }
            return redirect('/admin/add-images/'.$id)->with('flash_message_error', 'Vui lòng chọn ảnh!');
        }
        $productImages = ProductsImages::where(['product_id' => $id])->get();
        return view('backend.products.add_images')->with(compact('productDetails', 'productImages'));
    }

    public function deleteAltImage($id = null)
    {
        $productImage = ProductsImages::where(['id' => $id])->first();
        ProductsImages::where(['id' => $id])->delete();
        return redirect()->back()->with('flash_message_error', 'Xóa ảnh thành công!');
    }

// Controller Product Colors
    public function addColors(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            foreach ($data['sku'] as $key => $val) {
                if (!empty($val)) {
                    // Kiểm tra SKU đã tồn tại chưa
                    $attrCountSKU = ProductsColors::where('sku', $val)->count();
                    if ($attrCountSKU > 0) {
                        return redirect('/admin/add-colors/'. $id)->with('flash_message_error', 'SKU đã tồn tại, vui lòng chon SKU khác!');
                    }
                    // Kiểm tra màu có bị trùng không
                    $attrCountColors = ProductsColors::where(['product_id' => $id, 'color' => $data['color'][$key]])->count();
                    if ($attrCountColors > 0) {
                        return redirect('/admin/add-colors/'. $id)->with('flash_message_error', ''.$data['color'][$key].' Màu đã tồn tại vui lòng chọn màu khác!');
                    }

                    $color = new ProductsColors;
                    $color->product_id  = $id;
                    $color->sku         = $val;
                    $color->color       = $data['color'][$key];
                    $color->price       = $data['price'][$key];
                    $color->stock       = $data['stock'][$key];
                    $color->save();
                }
            }
            return redirect('/admin/add-colors/'. $id)->with('flash_message_success','Thêm màu thành công!');
        }
        $productDetails = Products::with('colors')->where(['id' => $id])->first();
        return view('backend.products.add_colors')->with(compact('productDetails'));
    }

    public function editColors(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            foreach ($data['attr'] as $key => $attr) {
                ProductsColors::where(['id' => $data['attr'][$key]])->update([
                    'sku'   => $data['sku'][$key],
                    'color' => $data['color'][$key],
                    'price' => $data['price'][$key],
                    'stock' => $data['stock'][$key],
                ]);
            }
            return redirect()->back()->with('flash_message_success', 'Cập nhật màu thành công!');
        }
    }

    public function deleteColors($id = null)
    {
        ProductsColors::where(['id' => $id])->delete();
        return redirect()->back()->with('flash_message_error', 'Xóa màu thành công!');
    }
}
