<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products;
use App\ProductsImages;
use App\ProductsAttributes;
use App\ProductsColors;
use Session;
use DB;

class ProductsController extends Controller
{
    public function products($id = null)
    {
        $productDetails = Products::with('attributes')->with('colors')->where('id', $id)->first();
        $productsAltImages = ProductsImages::where('product_id', $id)->get();
        return view('frontend.products.product_detail')->with(compact('productDetails', 'productsAltImages'));
    }

    public function getPriceSize(Request $request)
    {
        $html = '';
        $data = $request->all();
        $proAttr = explode("-", $data['idSize']);
        $productDetails = Products::where('id', $proAttr[0])->first();
        $proAttr = ProductsAttributes::where(['product_id' => $proAttr[0], 'size' => $proAttr[1]])->first();
        if ($productDetails['promotion'] > 0) {
            $html .= "Giá :";
            $html .= '<del style="color:#000;font-size:15px;">'.number_format($proAttr['price']).'đ</del> ';
            $html .= number_format(($proAttr['price']) * ((100-($productDetails['promotion']))/100)).'đ ';
            $html .= '<span class="badge badge-danger">Giảm '.$productDetails['promotion'].' %</span>';
            $html .= '<input type="hidden" id="product_price" name="product_price" value="'.$proAttr['price'].'">';
            return $html;
        } else {
            $html .= 'Giá : '.number_format($proAttr['price']).'đ';
            $html .= '<input type="hidden" id="product_price" name="product_price" value="'.$proAttr['price'].'">';
            return $html;
        }
    }

    public function getPriceColor(Request $request)
    {
        $html = '';
        $data = $request->all();
        $proAttr = explode("-", $data['idColor']);
        $productDetails = Products::where('id', $proAttr[0])->first();
        $proAttr = ProductsColors::where(['product_id' => $proAttr[0], 'color' => $proAttr[1]])->first();
        if ($productDetails['promotion'] > 0) {
            $html .= "Giá :";
            $html .= '<del style="color:#000;font-size:15px;">'.number_format($proAttr['price']).'đ</del> ';
            $html .= number_format(($proAttr['price']) * ((100-($productDetails['promotion']))/100)).'đ ';
            $html .= '<span class="badge badge-danger">Giảm '.$productDetails['promotion'].' %</span>';
            $html .= '<input type="hidden" id="product_price" name="product_price" value="'.$proAttr['price'].'">';
            return $html;
        } else {
            $html .= 'Giá : '.number_format($proAttr['price']).'đ';
            $html .= '<input type="hidden" id="product_price" name="product_price" value="'.$proAttr['price'].'">';
            return $html;
        }
    }

// Contronller Cart
    public function addtoCart(Request $request)
    {
        $data = $request->all();
        $session_id = Session::get('session_id');
        if (empty($session_id)) {
            $session_id = str_random(40);
            Session::put('session_id', $session_id);
        }
        $sizeAttr = explode('-', $data['size']);
        $colorAttr = explode('-', $data['color']);
        $countProducts = DB::table('cart')->where(['product_id'     => $data['product_id'],
                                                'price'             => $data['product_price'],
                                                'size'              => $sizeAttr[1],
                                                'color'             => $colorAttr[1],
                                                'session_id'        => $session_id
                                            ])->count();
        if ($countProducts > 0) {
            return 0;
        } else {
            DB::table('cart')->insert([
                'product_id'        => $data['product_id'],
                'product_name'      => $data['product_name'],
                'product_code'      => $data['product_code'],
                'price'             => $data['product_price'],
                'promotion'         => $data['promotion'],
                'size'              => $sizeAttr[1],
                'color'             => $colorAttr[1],
                'quantity'          => $data['quantity'],
                'session_id'        => $session_id
            ]);
            // lấy số lượng sản phẩm có trong giỏ hàng
            $countCart = DB::table('cart')->where(['session_id' => $session_id])->count();
            Session::put('countCart', $countCart);
            // tạo session hiển thị giỏ hàng trên header
            $userCart = DB::table('cart')->where('session_id', $session_id)->orderBy('id', 'desc')->get();
            foreach ($userCart as $key => $products) {
                $productDetails = Products::where('id', $products->product_id)->first();
                $userCart[$key]->image = $productDetails['image'];
            }
            Session::put('userCart', $userCart);
            $image = Products::select('image')->where('id', $data['product_id'])->first();
            $response = [];
            $response['countCart'] = $countCart;
            $response['image'] = $image;
            return json_encode($response);
        }
    }

    public function cart(Request $request)
    {
        $session_id = Session::get('session_id');
        $userCart = DB::table('cart')->where('session_id', $session_id)->orderBy('id', 'desc')->get();
        foreach ($userCart as $key => $products) {
            $productDetails = Products::with('attributes')->with('colors')->where('id', $products->product_id)->first();
            $userCart[$key]->image = $productDetails['image'];
            $userCart[$key]->attributes = $productDetails['attributes'];
            $userCart[$key]->colors = $productDetails['colors'];
        }
        return view('frontend.products.cart')->with(compact('userCart'));
    }

    public function updateCartQuantity($id = null, $quantity = null)
    {
        DB::table('cart')->where('id', $id)->increment('quantity', $quantity);
        // cập nhật danh sách sản phẩm để hiển thị trên cart header
        $session_id = Session::get('session_id');
        $userCart = DB::table('cart')->where('session_id', $session_id)->orderBy('id', 'desc')->get();
        foreach ($userCart as $key => $products) {
            $productDetails = Products::where('id', $products->product_id)->first();
            $userCart[$key]->image = $productDetails['image'];
        }
        Session::put('userCart', $userCart);
        return redirect('/cart')->with('flash_message_success','Cập nhật số lượng thành công');
    }

    public function updateCartColorSize(Request $request, $id = null)
    {
        $data = $request->all();
        DB::table('cart')->where('id', $id)->update([
            'color' => $data['color'],
            'size' => $data['size'],
        ]);
        // cập nhật danh sách sản phẩm để hiển thị trên cart header
        $session_id = Session::get('session_id');
        $userCart = DB::table('cart')->where('session_id', $session_id)->orderBy('id', 'desc')->get();
        foreach ($userCart as $key => $products) {
            $productDetails = Products::where('id', $products->product_id)->first();
            $userCart[$key]->image = $productDetails['image'];
        }
        Session::put('userCart', $userCart);
        return redirect('/cart')->with('flash_message_success','Cập nhật màu và size thành công');
    }

    public function deleteCartProduct($id = null)
    {
        DB::table('cart')->where('id', $id)->delete();
        // lấy số lượng sản phẩm có trong giỏ hàng để hiển thị trên icon cart
        $session_id = Session::get('session_id');
        $countCart = DB::table('cart')->where(['session_id' => $session_id])->count();
        Session::put('countCart', $countCart);
        // cập nhật danh sách sản phẩm để hiển thị trên cart header
        $session_id = Session::get('session_id');
        $userCart = DB::table('cart')->where('session_id', $session_id)->orderBy('id', 'desc')->get();
        foreach ($userCart as $key => $products) {
            $productDetails = Products::where('id', $products->product_id)->first();
            $userCart[$key]->image = $productDetails['image'];
        }
        Session::put('userCart', $userCart);
        return redirect('/cart')->with('flash_message_error','Xóa sản phẩm trong giỏ hàng thành công!');
    }
}
