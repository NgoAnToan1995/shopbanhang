<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route Frontend
    Route::match(['get', 'post'], '/', 'Frontend\IndexController@index');
    Route::match(['get', 'post'], '/home', 'Frontend\IndexController@index');
    Route::get('/categories/{category_id}', 'Frontend\IndexController@categories');
    Route::get('/products/{id}', 'Frontend\ProductsController@products');
    Route::get('/get-product-price-size', 'Frontend\ProductsController@getPriceSize');
    Route::get('/get-product-price-color', 'Frontend\ProductsController@getPriceColor');

    // Cart Route
    Route::match(['get', 'post'], '/add-cart', 'Frontend\ProductsController@addtoCart');
    Route::match(['get', 'post'], '/cart', 'Frontend\ProductsController@cart');
    Route::get('/cart/delete-product/{id}', 'Frontend\ProductsController@deleteCartProduct');
    Route::get('/cart/update-quantity/{id}/{quantity}', 'Frontend\ProductsController@updateCartQuantity');
    Route::post('/cart/update-color-size/{id}', 'Frontend\ProductsController@updateCartColorSize');

// Route Backend

    Auth::routes();

    Route::match(['get', 'post'], '/admin', 'Backend\AdminController@login')->name('admin');
    Route::group(['prefix' => 'admin', 'namespace' => 'Backend', 'middleware' => ['auth']], function () {
        // Route Dashboard
        Route::match(['get', 'post'], '/dashboard', 'AdminController@dashboard');

        // Category Route
        Route::match(['get', 'post'], '/add-category', 'CategoryController@addCategory');
        Route::match(['get', 'post'], '/view-categories', 'CategoryController@viewCategories');
        Route::match(['get', 'post'], '/edit-category/{id}', 'CategoryController@editCategory');
        Route::post('/delete-category', 'CategoryController@deleteCategory');
        Route::post('/update-category-status', 'CategoryController@updateStatus');

        // Product Route
        Route::match(['get', 'post'], '/add-product', 'ProductsController@addProduct');
        Route::match(['get', 'post'], '/view-products', 'ProductsController@viewProducts');
        Route::match(['get', 'post'], '/edit-product/{id}', 'ProductsController@editProduct');
        Route::match(['get', 'post'], '/delete-product/{id}', 'ProductsController@deleteProduct');
        Route::post('/update-product-status', 'ProductsController@updateStatus');

        // Route Products Attributes
        Route::match(['get', 'post'], '/add-attributes/{id}', 'ProductsController@addAttributes');
        Route::match(['get', 'post'], '/edit-attribute/{id}', 'ProductsController@editAttributes');
        Route::get('/delete-attribute/{id}', 'ProductsController@deleteAttribute');

        // Route Slider Image Products
        Route::match(['get', 'post'], '/add-images/{id}', 'ProductsController@addImages');
        Route::get('/delete-alt-image/{id}', 'ProductsController@deleteAltImage');

        // Route Products Colors
        Route::match(['get', 'post'], '/add-colors/{id}', 'ProductsController@addColors');
        Route::match(['get', 'post'], '/edit-colors/{id}', 'ProductsController@editColors');
        Route::get('/delete-colors/{id}', 'ProductsController@deleteColors');
    });
    Route::get('/logout', 'Backend\AdminController@logout');
