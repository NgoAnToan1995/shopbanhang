<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin Shop ban hàng</title>

        <link rel="shortcut icon" href="{{asset('public/backend/dist/img/ico/favicon.png')}}" type="image/x-icon">
        <link href="{{asset('public/backend/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('public/backend/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('public/backend/dist/css/stylecrm.css')}}" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <!-- Content Wrapper -->
        <div class="login-wrapper">
            <div class="container-center">
                @if (Session::has('flash_message_error'))
                    <div class="alert alert-sm alert-danger alert-block" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>{!! session('flash_message_error') !!}</strong>
                    </div>
                @endif

                @if (Session::has('flash_message_success'))
                    <div class="alert alert-sm alert-success alert-block" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>{!! session('flash_message_success') !!}</strong>
                    </div>
                @endif
                <div class="login-area">
                    <div class="panel panel-bd panel-custom">
                        <div class="panel-heading">
                            <div class="view-header">
                                <div class="header-icon">
                                    <i class="pe-7s-unlock"></i>
                                </div>
                                <div class="header-title">
                                    <h3>Đăng nhập</h3>
                                    <small><strong>Nhập thông tin của bạn để đăng nhập.</strong></small>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <form action="{{url('admin')}}" id="loginForm" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label class="control-label" for="email">Email</label>
                                    <input type="text" placeholder="example@gmail.com" title="Vui lòng nhập email" required="" name="email" id="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="password">Mật khẩu</label>
                                    <input type="password" title="Vui lòng nhập mật khẩu" placeholder="******" required="" name="password" id="password" class="form-control">
                                </div>
                                <div>
                                    <button class="btn btn-add">Đăng nhập</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{asset('public/backend/plugins/jQuery/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/backend/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    </body>
</html>
