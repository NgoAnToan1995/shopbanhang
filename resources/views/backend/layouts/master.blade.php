<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title') - Way Shop</title>
        <meta name="csrf-token" content="{{csrf_token()}}">
        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="{{asset('public/backend/dist/img/ico/favicon.png')}}" type="image/x-icon">
        <!-- Start Global Mandatory Style
            =====================================================================-->
        <!-- jquery-ui css -->
        <link href="{{asset('public/backend/plugins/jquery-ui-1.12.1/jquery-ui.min.css')}}" rel="stylesheet" type="text/css"/>
        <!-- Bootstrap -->
        <link href="{{asset('public/backend/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <!-- Bootstrap rtl -->
        <!-- Lobipanel css -->
        <link href="{{asset('public/backend/plugins/lobipanel/lobipanel.min.css')}}" rel="stylesheet" type="text/css"/>
        <!-- Pace css -->
        <link href="{{asset('public/backend/plugins/pace/flash.css')}}" rel="stylesheet" type="text/css"/>
        <!-- Font Awesome -->
        <link href="{{asset('public/backend/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
        <!-- Pe-icon -->
        <link href="{{asset('public/backend/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}" rel="stylesheet" type="text/css"/>
        <!-- Themify icons -->
        <link href="{{asset('public/backend/themify-icons/themify-icons.css')}}" rel="stylesheet" type="text/css"/>
        <!-- End Global Mandatory Style
            =====================================================================-->
        <!-- Start page Label Plugins
            =====================================================================-->
        <!-- Emojionearea -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
        <link href="{{asset('public/backend/plugins/emojionearea/emojionearea.min.css')}}" rel="stylesheet" type="text/css"/>
        <!-- Theme style -->
        <link href="{{asset('public/backend/dist/css/stylecrm.css')}}" rel="stylesheet" type="text/css"/>
        <!-- Theme style rtl -->
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- jquery-ui -->
        <script src="{{asset('public/backend/plugins/jquery-ui-1.12.1/jquery-ui.min.js')}}" type="text/javascript"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="{{asset('public/ckeditor/ckeditor.js')}}"></script>
        <script src="{{asset('public/ckfinder/ckfinder.js')}}"></script>
    </head>
    <body class="hold-transition sidebar-mini">
        <!--preloader-->
        <div id="preloader">
            <div id="status"></div>
        </div>
        <!-- Site wrapper -->
        <div class="wrapper">

            @include('backend.layouts.header')

            @include('backend.layouts.sidebar')

            @yield('content_backend')

            @include('backend.layouts.footer')

        </div>

        <script>
            $( function() {
                $( "#datepicker" ).datepicker({
                    minDate: 0,
                    dateFormat: 'yy-mm-dd'
                });
            } );
        </script>
        <!-- Bootstrap -->
        <script src="{{asset('public/backend/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
        <!-- lobipanel -->
        <script src="{{asset('public/backend/plugins/lobipanel/lobipanel.min.js')}}" type="text/javascript"></script>
        <!-- Pace js -->
        <script src="{{asset('public/backend/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
        <!-- SlimScroll -->
        <script src="{{asset('public/backend/plugins/slimScroll/jquery.slimscroll.min.js')}}" type="text/javascript">    </script>
        <!-- FastClick -->
        <script src="{{asset('public/backend/plugins/fastclick/fastclick.min.js')}}" type="text/javascript"></script>
        <!-- CRMadmin frame -->
        <script src="{{asset('public/backend/dist/js/custom.js')}}" type="text/javascript"></script>
        <!-- Dashboard js -->
        <script src="{{asset('public/backend/dist/js/dashboard.js')}}" type="text/javascript"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready( function () {
                $('#table_id').DataTable({
                    "paging":false
                });
                $(this).closest('td').attr('id');
                // ajax update product status
                $(".ProductStatus").change(function() {
                    var id = $(this).attr('rel');
                    if ($(this).prop("checked") == true) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type:  'post',
                            url: '/admin/update-product-status',
                            data: {status: '1', id: id},
                            success: function(resp) {
                                $("#message_success").show();
                                setTimeout(function() { $("#message_success").fadeOut('slow'); }, 2000)
                            },
                            error: function() {
                                alert("Error");
                            }
                        });
                    } else {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type:  'post',
                            url: '/admin/update-product-status',
                            data: {status: '0', id: id},
                            success: function(resp) {
                                $("#message_error").show();
                                setTimeout(function() { $("#message_error").fadeOut('slow'); }, 3000)
                            },
                            error: function() {
                                alert("Error");
                            }
                        });
                    }
                });

                // ajax update category status
                $(".CategoryStatus").change(function() {
                    var id = $(this).attr('rel');
                    if ($(this).prop("checked") == true) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type:  'post',
                            url: '/admin/update-category-status',
                            data: {status: '1', id: id},
                            success: function(resp) {
                                $("#message_success").show();
                                setTimeout(function() { $("#message_success").fadeOut('slow'); }, 2000)
                            },
                            error: function() {
                                alert("Error");
                            }
                        });
                    } else {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type:  'post',
                            url: '/admin/update-category-status',
                            data: {status: '0', id: id},
                            success: function(resp) {
                                $("#message_error").show();
                                setTimeout(function() { $("#message_error").fadeOut('slow'); }, 3000)
                            },
                            error: function() {
                                alert("Error");
                            }
                        });
                    }
                });
            });

            // Add Remove Fields Dynamically
            $(document).ready(function(){
                var maxField = 10; //Input fields increment limitation
                var addButton = $('.add_button'); //Add button selector
                var wrapper = $('.field_wrapper'); //Input field wrapper
                var fieldHTML = '<div style="display:flex;"><input type="text" name="sku[]" id="sku" placeholder="SKU" class="form-control" style="width: 120px;margin-right:5px;margin-top:5px;"/><input type="text" name="size[]" id="size" placeholder="Size" class="form-control" style="width: 120px;margin-right:5px;margin-top:5px;"/><input type="text" name="price[]" id="price" placeholder="Price" class="form-control" style="width: 120px;margin-right:5px;margin-top:5px;"/><input type="text" name="stock[]" id="stock" placeholder="Stock" class="form-control" style="width: 120px;margin-right:5px;margin-top:5px;"/><a href="javascript:void(0);"  style="padding-top: 10px;" class="remove_button">Xóa</a></div>'; //New input field html
                var x = 1; //Initial field counter is 1

                //Once add button is clicked
                $(addButton).click(function(){
                    //Check maximum number of input fields
                    if(x < maxField){
                        x++; //Increment field counter
                        $(wrapper).append(fieldHTML); //Add field html
                    }
                });

                // Button add color
                var addColor = $('.add_color'); //Add button selector
                var fieldColorHTML = '<div style="display:flex;"><input type="text" name="sku[]" id="sku" placeholder="SKU" class="form-control" style="width: 120px;margin-right:5px;margin-top:5px;"/><input type="text" name="color[]" id="color" placeholder="Color" class="form-control" style="width: 120px;margin-right:5px;margin-top:5px;"/><input type="text" name="price[]" id="price" placeholder="Price" class="form-control" style="width: 120px;margin-right:5px;margin-top:5px;"/><input type="text" name="stock[]" id="stock" placeholder="Stock" class="form-control" style="width: 120px;margin-right:5px;margin-top:5px;"/><a href="javascript:void(0);"  style="padding-top: 10px;" class="remove_button">Xóa</a></div>'; //New input field html
                //Once add button is clicked
                $(addColor).click(function(){
                    //Check maximum number of input fields
                    if(x < maxField){
                        x++; //Increment field counter
                        $(wrapper).append(fieldColorHTML); //Add field html
                    }
                });

                //Once remove button is clicked
                $(wrapper).on('click', '.remove_button', function(e){
                    e.preventDefault();
                    $(this).parent('div').remove(); //Remove field html
                    x--; //Decrement field counter
                });
            });

            function selectFileWithCKFinder( elementId ) {
                CKFinder.modal( {
                    chooseFiles: true,
                    width: 800,
                    height: 600,
                    onInit: function( finder ) {
                        finder.on( 'files:choose', function( evt ) {
                            var file = evt.data.files.first();
                            var output = document.getElementById( elementId );
                            output.value = file.getUrl();
                        } );

                        finder.on( 'file:choose:resizedImage', function( evt ) {
                            var output = document.getElementById( elementId );
                            output.value = evt.data.resizedUrl;
                        } );
                    }
                } );
            }

            function deleteCategory(id) {
                var action = confirm('Bạn có chắt muốn xóa sản phẩm này?');
                if (action == true) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'post',
                        url: '/admin/delete-category',
                        data: {id: id},
                        success: function(resp) {
                            if (resp == 0) {
                                $("#message_error_delete_category").show();
                                setTimeout(function() { $("#message_error_delete_category").fadeOut('slow'); }, 5000)
                            } else {
                                $("#message_success_delete_category").show();
                                setTimeout(function() {
                                    $("#message_success_delete_category").fadeOut('slow');
                                    location.reload();
                                }, 3000)
                            }
                        },
                        error: function() {
                            alert('Error');
                        }
                    });
                }
            }
        </script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.js"></script>
    </body>
</html>

