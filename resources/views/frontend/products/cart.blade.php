@extends('frontend.layouts.master')
@section('title', 'Giỏ hàng')
@section('trang-chu','active')
@section('content_frontend')
<!-- Start All Title Box -->
<div class="all-title-box">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Cart</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Shop</a></li>
                    <li class="breadcrumb-item active">Cart</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End All Title Box -->

<!-- Start Cart  -->
<div class="cart-box-main">
    @if (Session::has('flash_message_error'))
        <div class="alert alert-sm alert-danger alert-block" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{!! session('flash_message_error') !!}</strong>
        </div>
    @endif

    @if (Session::has('flash_message_success'))
        <div class="alert alert-sm alert-success alert-block" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{!! session('flash_message_success') !!}</strong>
        </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-main table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Ảnh</th>
                                <th>Tên sản phẩm</th>
                                <th></th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Tổng tiền</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total_amount = 0; ?>
                            @foreach ($userCart as $cart)
                                <tr>
                                    <td class="thumbnail-img">
                                        <a href="#">
                                            <img class="img-fluid" src="{{$cart->image}}" alt="" />
                                        </a>
                                    </td>
                                    <td class="name-pr">
                                        {{$cart->product_name}}
                                    </td>
                                    <td class="name-pr">
                                        <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#updateModal-{{$cart->id}}">Màu: {{$cart->color}} <br> Size: {{$cart->size}}</button>
                                        <div class="modal fade" id="updateModal-{{$cart->id}}" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h2 class="modal-title" id="ModalLabel">Cập nhật size và màu</h2>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="{{url('/cart/update-color-size/'.$cart->id)}}" method="POST">
                                                            @csrf
                                                            <div class="form-group">
                                                                <label for="recipient-name" class="col-form-label">Màu:</label>
                                                                <select name="color" class="form-control">
                                                                    @foreach ($cart->colors as $colors)
                                                                        @if ($colors->color == $cart->color)
                                                                            <option value="{{$colors->color}}" selected>{{$colors->color}}</option>
                                                                        @else
                                                                            <option value="{{$colors->color}}">{{$colors->color}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="recipient-name" class="col-form-label">Size:</label>
                                                                <select name="size" class="form-control">
                                                                    @foreach ($cart->attributes as $sizes)
                                                                        @if ($sizes->size == $cart->size)
                                                                            <option value="{{$sizes->size}}" selected>{{$sizes->size}}</option>
                                                                        @else
                                                                            <option value="{{$sizes->size}}">{{$sizes->size}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <button type="submit" class="btn btn-primary float-right">Cập nhật</button>
                                                            <button type="button" class="btn btn-secondary float-right" style="margin-right:10px;" data-dismiss="modal">Trở lại</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="price-pr">
                                        @if ($cart->promotion > 0)
                                            <p><del>{{number_format($cart->price)}}đ</del> {{number_format(($cart->price) * ((100-($cart->promotion))/100))}}đ</p>
                                        @else
                                            <p>{{number_format($cart->price)}}đ</p>
                                        @endif

                                    </td>
                                    <td class="quantity-box">
                                        @if ($cart->quantity>1)
                                            <a href="{{url('/cart/update-quantity/'.$cart->id.'/-1')}}" style="font-size: 25px;">-</a>
                                        @endif
                                        <input type="number" size="4" value="{{$cart->quantity}}" min="0" step="1" class="c-input-text qty text" style="width:40%;">
                                        <a href="{{url('/cart/update-quantity/'.$cart->id.'/1')}}" style="font-size: 25px;">+</a>
                                    </td>
                                    <td class="total-pr">
                                        <p>{{number_format(($cart->price) * ((100-($cart->promotion))/100) * $cart->quantity)}} đ</p>
                                    </td>
                                    <td class="remove-pr">
                                        <a href="{{url('/cart/delete-product/'. $cart->id)}}">
                                            <i class="fas fa-times"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                                    $total_amount += (($cart->price) * ((100-($cart->promotion))/100) * $cart->quantity);
                                ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row my-5">
            <div class="col-lg-6 col-sm-6">
                <div class="coupon-box">

                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="order-box">
                    <h3 style="font-size:22px;">Đơn hàng</h3>
                    <div class="d-flex gr-total">
                        <h5>Thành tiền</h5>
                        <div class="ml-auto h5"> <?php echo number_format($total_amount); ?> đ</div>
                    </div>
                </div>
                <div class="col-12 d-flex shopping-box pt-4" style="padding-right:0;"><a href="#" class="ml-auto btn hvr-hover">Thanh toán</a> </div>
            </div>
        </div>
    </div>
</div>
<!-- End Cart -->
@endsection
