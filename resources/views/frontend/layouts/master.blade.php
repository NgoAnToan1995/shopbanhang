<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Site Metas -->
    <title>@yield('title')</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="{{asset('public/frontend/images/favicon.ico')}}" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/css/bootstrap.min.css')}}">
    <!-- Site CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/css/style.css')}}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/css/responsive.css')}}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/css/custom.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

    @include('frontend.layouts.header')
    @yield('content_frontend')
    @include('frontend.layouts.footer')

    <a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

    <!-- ALL JS FILES -->
    <script src="{{asset('public/frontend/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('public/frontend/js/jquery-ui.js')}}"></script>
    <script src="{{asset('public/frontend/js/popper.min.js')}}"></script>
    <script src="{{asset('public/frontend/js/bootstrap.min.js')}}"></script>
    <!-- ALL PLUGINS -->
    <script src="{{asset('public/frontend/js/jquery.superslides.min.js')}}"></script>
    <script src="{{asset('public/frontend/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('public/frontend/js/inewsticker.js')}}"></script>
    <script src="{{asset('public/frontend/js/bootsnav.js')}}"></script>
    <script src="{{asset('public/frontend/js/images-loded.min.js')}}"></script>
    <script src="{{asset('public/frontend/js/isotope.min.js')}}"></script>
    <script src="{{asset('public/frontend/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('public/frontend/js/baguetteBox.min.js')}}"></script>
    <script src="{{asset('public/frontend/js/form-validator.min.js')}}"></script>
    <script src="{{asset('public/frontend/js/contact-form-script.js')}}"></script>
    <script src="{{asset('public/frontend/js/custom.js')}}"></script>
    <script>
        $(document).ready(function() {
            // thay đổi giá khi chọn size
            $("#selSize").change(function() {
                $(".errorSize").html('');
                var idSize = $(this).val();
                if (idSize == "" || idSize == "0") {
                    return false;
                }
                $.ajax({
                    type: 'get',
                    url: '/get-product-price-size',
                    data: {idSize: idSize},
                    success: function(resp) {
                        $('#getPrice').html(resp);
                        // $('#product_price').val(arr[0]);
                    },
                    error: function() {
                        alert('Error');
                    }
                });
            });

            // thay đổi giá khi chọn màu
            $("#selColor").change(function() {
                $(".errorColor").html('');
                var idColor = $(this).val();
                if (idColor == "" || idColor == "0") {
                    return false;
                }
                $.ajax({
                    type: 'get',
                    url: '/get-product-price-color',
                    data: {idColor: idColor},
                    success: function(resp) {
                        $('#getPrice').html(resp);
                        // $('#product_price').val(arr[0]);
                    },
                    error: function() {
                        alert('Error');
                    }
                });
            });

            $("#quantity").change(function() {
                $(".errorQuantity").html('');
            });
        });

        function selectPaymentMethod() {
            if ($('.stripe').is(':checked') || $('.cod').is(':checked') || $('.paypal').is(':checked')) {
                // alert('checked');
            } else {
                alert('Vui lòng chọn phương thức thanh toán!');
                return false;
            }
        }

        // submit add to cart
        function submitAddtocart() {
            var size = $("#selSize").val();
            var color = $("#selColor").val();
            var quantity = $("#quantity").val();
            if (size == 0) {
                $(".errorSize").html('Vui lòng chọn size');
                return false;
            }

            if (color == 0) {
                $(".errorColor").html('Vui lòng chọn màu');
                return false;
            }

            if (quantity <= 0) {
                $(".errorQuantity").html('Vui lòng chọn số lượng');
                return false;
            }

            // ajax add to cart
            if (size != 0 && color != 0 && quantity != 0) {
                var product_id      = $('#product_id').val();
                var product_name    = $('#product_name').val();
                var product_code    = $('#product_code').val();
                var product_price   = $('#product_price').val();
                var promotion       = $('#promotion').val();
                var product_size    = $('#selSize').val();
                var product_color   = $('#selColor').val();
                var total = $('#totalHeader').html();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'post',
                    url: '/add-cart',
                    data: {
                        product_id: product_id,
                        product_name: product_name,
                        product_code: product_code,
                        product_price: product_price,
                        promotion: promotion,
                        size: product_size,
                        color: product_color,
                        quantity: quantity,
                    },
                    success: function(resp) {
                        var response = JSON.parse(resp);
                        if (response != 0) {
                            $('.countCart').html(response.countCart);
                            $('.side').addClass('on');
                            // lây giá khuyến mãi hiển thị trên header cart nếu % khuyến mãi > 0
                            if (promotion > 0) {
                                product_price = (product_price * ((100-(promotion))/100));
                            }
                            // thêm sản phẩm vào header cart
                            var link = '{{url('/products/')}}' + '/' + product_id;
                            var product = `<li>
                                            <a href="#" class="photo"><img src="`+response.image.image+`" class="cart-thumb" alt="" /></a>
                                            <h6><a href="`+ link +`">` + product_name + `</a></h6>
                                            <p><span class="price">` + formatNumber(product_price) + `đ</span> - x` + quantity + `</p>
                                        </li>`;
                            var cartHeader = $('.cart-list').html();
                            $('.cart-list').html(product + cartHeader);
                            // thay đổi tổng tiền trên header cart
                            var total = $('#totalHeader').html();
                            $('#totalHeader').html(formatNumber(parseInt(total.replace(',','')) + (product_price*quantity)))
                        }
                    },
                    error: function() {
                        alert('Error');
                    }
                });
            }
        }

        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        }
    </script>
</body>

</html>
