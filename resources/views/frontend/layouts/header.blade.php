<header class="main-header">
    <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
                <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('public/frontend/images/logo.png')}}" class="logo" alt=""></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                    <li class="nav-item @yield('trang-chu')"><a class="nav-link" href="{{url('/')}}">Trang chủ</a></li>
                    <li class="nav-item @yield('gioi-thieu')"><a class="nav-link" href="{{url('/about-us')}}">Giới thiệu</a></li>
                    <li class="nav-item @yield('lien-he')"><a class="nav-link" href="{{url('/contact-us')}}">Liên hệ</a></li>
                </ul>
            </div>
            <div class="attr-nav">
                <ul>
                    <li class="side-menu">
                        <a href="javascript:void(0)">
                            <i class="fa fa-shopping-bag"></i>
                            <span class="badge countCart">
                                @if (Session::get('countCart'))
                                    {{Session::get('countCart')}}
                                @endif
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="side">
            <a href="#" class="close-side"><i class="fa fa-times"></i></a>
            <li class="cart-box">
                <ul class="cart-list">
                    <?php
                        $total = 0;
                        if (Session::get('userCart')) {
                            $carts = Session::get('userCart');
                            foreach ($carts as $cart) {
                                $total += ($cart->price * $cart->quantity);
                                ?>
                                    <li>
                                        <a href="#" class="photo"><img src="{{$cart->image}}" class="cart-thumb" alt="Image" /></a>
                                        <h6><a href="{{url('/products/'. $cart->product_id)}}">{{$cart->product_name}} </a></h6>
                                        <p><span class="price">{{number_format($cart->price)}}đ</span> - x{{$cart->quantity}}</p>
                                    </li>
                                <?php
                            }
                        }
                    ?>
                    <li class="total">
                        <a href="{{url('/cart')}}" class="btn btn-default hvr-hover btn-cart">Giỏ hàng</a>
                        <span class="float-right"><strong>Tổng</strong>: <small id="totalHeader">{{number_format($total)}}</small> đ</span>
                    </li>
                </ul>
            </li>
        </div>
    </nav>
</header>
